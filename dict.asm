%include "lib.inc"

section .data

pointer: dq 0

section .text

; Принимает указатель на нуль-терминированную строку в rsi и указатель на начало словаря в rdi
; Возвращает адрес начала вхождения ключа в словарь, если он существует, иначе 0

global find_word

find_word:
	push rbx
.loop:
	mov rbx, rdi
	lea rdi, [rdi + 8]
	call string_equals
	cmp rax, 1
	je .end
	mov rdi, qword[rbx]
	test rdi, rdi
	je .fail
	jmp .loop
.fail:
	xor rdi, rdi
.end:
	mov rax, rdi
	pop rbx
	ret
