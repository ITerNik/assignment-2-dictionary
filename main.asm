%include "colon.inc"

%include "dict.inc"
%include "lib.inc"

%define BUF_LENGTH 256

section .rodata

%include "words.inc"

err_message: db `Key not found\0`
ov_message: db `Key length is bigger than buffer size\0`

section .bss

str: resb BUF_LENGTH

section .text
global _start

_start:
	mov rsi, BUF_LENGTH
	mov rdi, str
	call read_line
	test rax, rax
	je .overflow
	mov rdi, last
	mov rsi, rax
	call find_word
	test rax, rax
	je .err
	push rax
	call string_length
	pop rdi
	lea rdi, [rdi + rax + 1]
	call print_string
	call exit

.overflow:
	mov rdi, ov_message
	jmp .end
.err:
	mov rdi, err_message
.end:
	call print_err
	call exit


