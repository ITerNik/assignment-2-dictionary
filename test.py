#!/usr/bin/python3

__unittest = True

import subprocess
import unittest
import xmlrunner
from subprocess import CalledProcessError, Popen, PIPE

test_dict = {'first word': 'first word explanation', 'second word': 'second word explanation', 'third word': 'third word explanation'}

class DictTest(unittest.TestCase):

    def compile(self, fname):
        self.assertEqual(subprocess.call( ['nasm', '-f', 'elf64', fname + '.asm', '-o', fname+'.o'] ), 0, 'failed to compile')
        self.assertEqual(subprocess.call( ['ld', '-o' , fname, fname+'.o', 'lib.o', 'dict.o'] ), 0, 'failed to link')

    def launch(self, fname, std_input):
        output = ''
        try:
            p = Popen(['./'+fname], shell=None, stdin=PIPE, stdout=PIPE, stderr=PIPE)
            (output, error) = p.communicate(std_input.encode())
            self.assertNotEqual(p.returncode, -11, 'segmentation fault')
            return (output.decode(), error.decode())
        except CalledProcessError as exc:
            self.assertNotEqual(exc.returncode, -11, 'segmentation fault')
            return (exc.output.decode(), exc.returncode)

    def perform(self, fname, input):
        self.compile(fname)
        return self.launch(fname, input)

    def test_key(self):
        inputs = ["second word", "third word"]
        for input in inputs:
            (output, error) = self.perform('main', input)
            self.assertIn(test_dict[input], output, 'find_word(%s) should return %s' % (input, test_dict[input]))

    def test_key_too_long(self):
        inputs = ["limited length " * 18]
        for input in inputs:
            (output, error) = self.perform('main', input)
            self.assertEqual(error, 'Key length is bigger than buffer size', 'find_word from string of length more then 256 should return error')

    def test_unexisting_key(self):
        inputs = ["forth word"]
        for input in inputs:
            (output, error) = self.perform('main', input)
            self.assertIn("Key not found", error, 'find_word(%s) should return error' % (input))

if __name__ == "__main__":
    with open('report.xml', 'w') as report:
        unittest.main(testRunner=xmlrunner.XMLTestRunner(output=report), failfast=False, buffer=False, catchbreak=False)

